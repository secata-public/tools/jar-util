package com.secata.jarutil;

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.jarutil.testclasses.ClassWithInnerClass;
import com.secata.jarutil.testclasses.MotherOfAllClasses;
import com.secata.tools.coverage.WithResource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

/** Test for {@link JarBuilder}. */
public class JarBuilderTest {

  private static final int[] CLASS_HEADER = {(byte) 0xCA, 0xFE, 0xBA, 0xBE};

  /** Class paths are correctly escaped. */
  @Test
  void nameToPath() {
    String name = JarBuilder.nameToPath("com.example.Class");
    assertThat(name).isEqualTo("com/example/Class");
  }

  /** Jar files can be created with both main classes and auxillery classes. */
  @Test
  void jar_built_with_two_classes_contains_both_classes_and_refers_correctly_to_the_main_class()
      throws IOException {
    byte[] rawJar = JarBuilder.buildJar(ClassWithInnerClass.class, JarBuilder.class);

    ZipInputStream in =
        new ZipInputStream(new ByteArrayInputStream(rawJar), StandardCharsets.UTF_8);

    ZipEntry entry = in.getNextEntry();
    assertThat(entry.getName()).isEqualTo("main");

    assertThat(new String(in.readAllBytes(), StandardCharsets.UTF_8))
        .isEqualTo("com.secata.jarutil.testclasses.ClassWithInnerClass");

    findEntryWithNameOrThrow(in, "com/");
    findEntryWithNameOrThrow(in, "com/secata/");
    findEntryWithNameOrThrow(in, "com/secata/jarutil/");
    findEntryWithNameOrThrow(in, "com/secata/jarutil/testclasses/");
    findEntryWithNameOrThrow(in, "com/secata/jarutil/testclasses/ClassWithInnerClass.class");
    assertThat(in.readAllBytes()).startsWith(CLASS_HEADER);

    findEntryWithNameOrThrow(
        in, "com/secata/jarutil/testclasses/ClassWithInnerClass$InnerClazz.class");
    assertThat(in.readAllBytes()).startsWith(CLASS_HEADER);

    findEntryWithNameOrThrow(in, "com/secata/jarutil/JarBuilder.class");

    byte[] expectedBytes =
        WithResource.apply(
            () -> {
              String name = "/com/secata/jarutil/JarBuilder.class";
              return JarBuilder.class.getResourceAsStream(name);
            },
            InputStream::readAllBytes,
            "error");

    byte[] actualBytes = in.readAllBytes();
    assertThat(actualBytes).containsExactly(expectedBytes);
  }

  /**
   * Additional classes will also have their recursively nested classes included in the jar file.
   */
  @Test
  void additionalClassesSupportNested() throws IOException {
    byte[] rawJar =
        JarBuilder.buildJar(JarBuilder.class, ClassWithInnerClass.class, MotherOfAllClasses.class);

    ZipInputStream in =
        new ZipInputStream(new ByteArrayInputStream(rawJar), StandardCharsets.UTF_8);

    final List<String> entryNames = getAllEntryNames(in);
    assertThat(entryNames)
        .containsExactly(
            "main",
            "com/",
            "com/secata/",
            "com/secata/jarutil/",
            "com/secata/jarutil/JarBuilder.class",
            "com/secata/jarutil/testclasses/",
            "com/secata/jarutil/testclasses/ClassWithInnerClass.class",
            "com/secata/jarutil/testclasses/ClassWithInnerClass$InnerClazz.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$Interface.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$Interface$NestedInterface.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$Interface"
                + "$NestedInterface$NestedEnum.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$FinalClass.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$AbstractClass.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$AbstractClass$NestedRecord.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$InnerRecord.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$InnerEnum.class");
  }

  /** Specifying nested classes explicitly does not result in duplicate entries. */
  @Test
  void noDuplicateClassesEmitted() throws IOException {
    byte[] rawJar =
        JarBuilder.buildJar(
            ClassWithInnerClass.class,
            MotherOfAllClasses.Interface.class,
            ClassWithInnerClass.InnerClazz.class,
            MotherOfAllClasses.class);

    ZipInputStream in =
        new ZipInputStream(new ByteArrayInputStream(rawJar), StandardCharsets.UTF_8);

    final List<String> entryNames = getAllEntryNames(in);
    assertThat(entryNames)
        .containsExactly(
            "main",
            "com/",
            "com/secata/",
            "com/secata/jarutil/",
            "com/secata/jarutil/testclasses/",
            "com/secata/jarutil/testclasses/ClassWithInnerClass.class",
            "com/secata/jarutil/testclasses/ClassWithInnerClass$InnerClazz.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$Interface.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$Interface$NestedInterface.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$Interface"
                + "$NestedInterface$NestedEnum.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$FinalClass.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$AbstractClass.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$AbstractClass$NestedRecord.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$InnerRecord.class",
            "com/secata/jarutil/testclasses/MotherOfAllClasses$InnerEnum.class");
  }

  /** Jar built with timestamp has same timestamp in the zip entries. */
  @Test
  void jar_built_with_timestamp_has_same_timestamp_in_the_zip_entries() throws IOException {
    long zipEntryTimestampSeconds = 1_506_862_123;
    byte[] rawJar =
        JarBuilder.buildJar(zipEntryTimestampSeconds, ClassWithInnerClass.class, JarBuilder.class);

    ZipInputStream in = new ZipInputStream(new ByteArrayInputStream(rawJar));

    ZipEntry entry;
    while ((entry = in.getNextEntry()) != null) {
      long timestampMillis = entry.getTime();
      assertThat(timestampMillis).isEqualTo(zipEntryTimestampSeconds * 1000);
    }
  }

  /** Different timestamps yields different jars. */
  @Test
  void different_timestamps_yields_different_jars() {
    byte[] timestamped1 = JarBuilder.buildJar(1_506_862_123L, ClassWithInnerClass.class);
    byte[] timestamped2 = JarBuilder.buildJar(2_506_862_123L, ClassWithInnerClass.class);

    assertThat(timestamped2).isNotEqualTo(timestamped1);
  }

  /** Same timestamps yields same jar. */
  @Test
  void same_timestamps_yields_same_jar() {
    byte[] timestamped1 = JarBuilder.buildJar(2_222_333_444L, ClassWithInnerClass.class);
    byte[] timestamped2 = JarBuilder.buildJar(2_222_333_444L, ClassWithInnerClass.class);

    assertThat(timestamped2).isEqualTo(timestamped1);
  }

  /** Jars built using system time have modified times close to now. */
  @Test
  void jars_built_using_system_time_have_modified_times_close_to_now() throws IOException {
    long millisDividedBy1000 = System.currentTimeMillis() / 1000L;
    long nowSeconds = millisDividedBy1000 * 1000L;

    byte[] zip = JarBuilder.buildJar(ClassWithInnerClass.class);
    ZipInputStream in = new ZipInputStream(new ByteArrayInputStream(zip));

    ZipEntry entry;
    while ((entry = in.getNextEntry()) != null) {
      long time = entry.getTime();
      assertThat(time).isCloseTo(nowSeconds, Offset.offset(1001L));
    }
  }

  private static void findEntryWithNameOrThrow(ZipInputStream in, String name) throws IOException {
    ZipEntry entry;
    while ((entry = in.getNextEntry()) != null) {
      if (entry.getName().equals(name)) {
        return;
      }
    }
    throw new RuntimeException("Could not find expected entry!");
  }

  private static List<String> getAllEntryNames(ZipInputStream in) throws IOException {
    final List<String> entryNames = new ArrayList<>();
    ZipEntry entry;
    while ((entry = in.getNextEntry()) != null) {
      entryNames.add(entry.getName());
    }
    return entryNames;
  }
}
