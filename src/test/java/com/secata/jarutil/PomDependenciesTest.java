package com.secata.jarutil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

final class PomDependenciesTest {

  @TempDir Path temporaryFolder;

  @BeforeEach
  void beforeEach() {
    assertThat(Files.isDirectory(this.temporaryFolder)).isTrue();
  }

  @Test
  void defaultPom() {
    PomDependencies dependencies = PomDependencies.readFromPom(false);
    assertThat(dependencies.lookup("com.secata.pom", "root", Dependency.Type.POM)).isNotNull();
    assertThat(dependencies.lookup("org.apache.maven", "maven-model")).isNotNull();
    assertThat(dependencies.model().getBuild()).isNull();

    for (Dependency dependency : dependencies.dependencies()) {
      assertThat(dependency.readFromRepository()).isNotNull();
    }
  }

  @Test
  void readPomValid() {
    File pomFile = setupPomInTempFolder();
    PomDependencies dependencies = PomDependencies.readFromPom(pomFile);
    assertThat(dependencies.dependencies()).hasSize(7);
    assertThat(dependencies.lookup("test.secata.pom", "jar", Dependency.Type.POM)).isNotNull();
    assertThat(dependencies.lookup("test.apache.maven", "maven-model")).isNotNull();
    assertThat(dependencies.lookup("test.something", "web")).isNotNull();
    assertThat(dependencies.lookup("com.secata.tools", "jartype", Dependency.Type.JAR)).isNotNull();
    assertThat(dependencies.lookup("com.secata.tools", "pomtype", Dependency.Type.POM)).isNotNull();
    assertThat(dependencies.lookup("com.secata.tools", "jartype", Dependency.Type.TESTS))
        .isNotNull();

    assertThat(dependencies.lookup("com.offForPITest", "jartype")).isNull();
    assertThat(dependencies.lookup("com.secata.tools", "jartype", Dependency.Type.POM)).isNull();
    assertThat(dependencies.lookup("com.secata.tools", "pomtype", Dependency.Type.JAR)).isNull();
    assertThat(dependencies.lookup("com.secata.tools", "pomtype", Dependency.Type.TESTS)).isNull();
  }

  /** Properties can be overridden. */
  @Test
  void childModulesCanOverrideProperties() {
    File pomFile = setupPomInTempFolder("test-module/pom.xml", "module-parent-pom.xml");

    PomDependencies dependencies = PomDependencies.readFromPom(pomFile, true);

    Dependency dependency = dependencies.lookup("test.secata.tools", "coverage");
    assertThat(dependency).isNotNull();
    assertThat(dependency.getVersion()).isEqualTo("2.3.6");
  }

  /**
   * Test that dependencies declared in a module can resolve versions declared in properties in the
   * module parent.
   */
  @Test
  void childModulesCanResolveParentProperties() {
    File pomFile = setupPomInTempFolder("test-module/pom.xml", "module-parent-pom.xml");

    PomDependencies dependencies = PomDependencies.readFromPom(pomFile, true);

    Dependency dependency = dependencies.lookup("test.apache.maven", "maven-model");
    assertThat(dependency).isNotNull();
    assertThat(dependency.getVersion()).isEqualTo("3.6.3");
  }

  /** Test that a dependency declaring ${project.version} resolves to the correct version. */
  @Test
  void dependencyWithProjectVersion() {
    File pomFile = setupPomInTempFolder("test-module/pom.xml", "module-parent-pom.xml");
    PomDependencies dependencies = PomDependencies.readFromPom(pomFile, true);

    Dependency dependency = dependencies.lookup("com.secata.tools", "jartype-delta");
    assertThat(dependency).isNotNull();
    assertThat(dependency.getVersion()).isEqualTo("2.3.14");
  }

  @Test
  void readPomValidWithSimpleParent() {
    File pomFile =
        setupPomInTempFolder("test-module/pom-simple.xml", "simple-module-parent-pom.xml");

    PomDependencies dependencies = PomDependencies.readFromPom(pomFile, true);
    List<Dependency> actual = dependencies.dependencies();
    assertThat(actual).hasSize(7);

    assertThat(dependencies.lookup("test.something", "web")).isNotNull();
    assertThat(dependencies.lookup("com.secata.tools", "jartype", Dependency.Type.JAR)).isNotNull();
    assertThat(dependencies.lookup("test.secata.pom", "jar", Dependency.Type.POM)).isNotNull();
    assertThat(dependencies.lookup("com.secata.tools", "pomtype", Dependency.Type.POM)).isNotNull();
    assertThat(dependencies.lookup("com.secata.tools", "jartype", Dependency.Type.TESTS))
        .isNotNull();
  }

  @Test
  void readPomValidWithParent() {
    File pomFile = setupPomInTempFolder("test-module/pom.xml", "module-parent-pom.xml");

    PomDependencies dependencies = PomDependencies.readFromPom(pomFile, true);
    List<Dependency> actual = dependencies.dependencies();
    assertThat(actual).hasSize(10);

    assertThat(dependencies.lookup("test.secata.pom", "jar-parent", Dependency.Type.POM))
        .isNotNull();

    assertThat(dependencies.lookup("test.something", "web")).isNotNull();
    assertThat(dependencies.lookup("com.secata.tools", "jartype", Dependency.Type.JAR)).isNotNull();
  }

  @Test
  void readSimplePomValid() {
    File pomFile = setupPomInTempFolder("simple_pom.xml");
    PomDependencies dependencies = PomDependencies.readFromPom(pomFile, false);
    assertThat(dependencies.dependencies()).isEmpty();
    assertThat(dependencies.model().getArtifactId()).isEqualTo("jar-util");
    assertThat(dependencies.model().getGroupId()).isEqualTo("com.secata.tools");
  }

  @Test
  void readSimplePomValidWithParent() {
    File pomFile = setupPomInTempFolder("simple_pom.xml");
    PomDependencies dependencies = PomDependencies.readFromPom(pomFile, true);
    assertThat(dependencies.dependencies()).isEmpty();
    assertThat(dependencies.model().getArtifactId()).isEqualTo("jar-util");
    assertThat(dependencies.model().getGroupId()).isEqualTo("com.secata.tools");
  }

  @Test
  void readPomInvalid() {
    assertThatThrownBy(
            () ->
                PomDependencies.readFromPom(
                    new File(temporaryFolder.getRoot().toString(), "nowhere"), false))
        .hasCauseInstanceOf(FileNotFoundException.class)
        .hasMessageContaining("IO Exception when reading model from pom file");
  }

  @Test
  void readPomWithoutAncestorsCurrentProject() {
    PomDependencies pomDependencies = PomDependencies.readFromPom();
    expectLocalPomDependencies(pomDependencies);
  }

  @Test
  void readPomWithIncludeAncestorsCurrentProject() {
    PomDependencies pomDependencies = PomDependencies.readFromPom(true);
    expectLocalPomDependencies(pomDependencies);
  }

  private static void expectLocalPomDependencies(PomDependencies pomDependencies) {
    List<Dependency> dependencies = pomDependencies.dependencies();
    List<String> dependencyIds = dependencies.stream().map(Dependency::getArtifactId).toList();
    checkDependencies(List.of("coverage", "maven-model"), dependencyIds, dependencies);
  }

  private static void checkDependencies(
      List<String> expectedDependencies,
      List<String> dependencyIds,
      List<Dependency> dependencies) {
    for (String id : expectedDependencies) {
      assertThat(dependencyIds.contains(id)).isTrue();
    }

    for (Dependency dependency : dependencies) {
      assertThat(dependency.getVersion()).isNotNull();
      assertThat(dependency.getVersion().contains("$")).isFalse();
    }
  }

  private File setupPomInTempFolder() {
    return setupPomInTempFolder("test-module/pom.xml");
  }

  private File setupPomInTempFolder(String... names) {
    File tempDir = temporaryFolder.toFile();
    for (String name : names) {
      File pomFile = new File(tempDir, name);
      pomFile.getParentFile().mkdirs();

      byte[] buffer =
          WithResource.apply(
              () -> getClass().getResourceAsStream(name),
              resourceAsStream -> {
                byte[] bytes = new byte[resourceAsStream.available()];
                assertThat(resourceAsStream.read(bytes)).isEqualTo(bytes.length);
                return bytes;
              },
              "Setup problem");
      WithResource.accept(
          () -> new FileOutputStream(pomFile), outStream -> outStream.write(buffer), "");
    }
    return new File(tempDir, names[0]);
  }
}
