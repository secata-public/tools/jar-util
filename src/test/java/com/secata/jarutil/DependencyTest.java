package com.secata.jarutil;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.util.Map;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public class DependencyTest {

  @Test
  public void equals() {
    EqualsVerifier.forClass(Dependency.class).verify();
  }

  @Test
  public void values() {
    Dependency dependency = new Dependency("group", "artifact", "version");
    assertThat(dependency.getGroupId()).isEqualTo("group");
    assertThat(dependency.getArtifactId()).isEqualTo("artifact");
    assertThat(dependency.getVersion()).isEqualTo("version");
    assertThat(dependency.toString()).isEqualTo("Dependency{'group.artifact.version'}");
    assertThat(dependency.toJarFilename()).isEqualTo("artifact-version.jar");
    assertThat(dependency.toRepositoryPath().toString()).endsWith("artifact-version.jar");
  }

  @Test
  public void toStringTest() {
    Dependency dependency = new Dependency("group", "artifact", "version", Dependency.Type.TESTS);
    assertThat(dependency.getGroupId()).isEqualTo("group");
    assertThat(dependency.getArtifactId()).isEqualTo("artifact");
    assertThat(dependency.getVersion()).isEqualTo("version");
    assertThat(dependency.toString()).isEqualTo("Dependency{'group.artifact.version' (TESTS)}");
    assertThat(dependency.toJarFilename()).isEqualTo("artifact-version-tests.jar");
    assertThat(dependency.toRepositoryPath().toString()).endsWith("artifact-version-tests.jar");
  }

  @Test
  public void resolveLocalRepositoryExplicitLocalRepository() {
    String expectedRepository = "/my/maven/path";
    Map<String, String> env = Map.of("LOCAL_MAVEN_REPOSITORY", expectedRepository);
    assertThat(Dependency.resolveLocalRepository(env::get)).isEqualTo(expectedRepository);
  }

  @Test
  public void resolveLocalRepositoryMatchingMavenOpts() {
    String expectedRepository = "/my/maven/path";
    Map<String, String> env =
        Map.of("MAVEN_OPTS", "-DsomeOpts -Dmaven.repo.local=" + expectedRepository);
    assertThat(Dependency.resolveLocalRepository(env::get)).isEqualTo(expectedRepository);
  }

  @Test
  public void resolveLocalRepositoryHomeDirWithMavenOpts() {
    String expectedRepository = "-DsomeMavenOpt=foo";
    Map<String, String> env = Map.of("MAVEN_OPTS", expectedRepository);
    assertThat(Dependency.resolveLocalRepository(env::get))
        .isEqualTo(System.getProperty("user.home") + "/.m2/repository");
  }

  @Test
  public void resolveLocalRepositoryHomeDir() {
    Map<String, String> env = Map.of();
    assertThat(Dependency.resolveLocalRepository(env::get))
        .isEqualTo(System.getProperty("user.home") + "/.m2/repository");
  }

  @Test
  public void resolveLocalRepository() {
    String defaultRepository = Dependency.resolveLocalRepository();
    assertThat(defaultRepository).isNotNull().isNotEmpty();
    assertThat(Path.of(defaultRepository)).isDirectory();
  }

  @Test
  public void unknownType() {
    Assertions.assertThatThrownBy(() -> Dependency.convertType("nothing"))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("nothing");
  }
}
