package com.secata.jarutil.testclasses;

/** Test utility for {@link JarBuilderTest}. */
public class MotherOfAllClasses {

  private final Interface.NestedInterface.NestedEnum value =
      Interface.NestedInterface.NestedEnum.SECOND;

  /** Example of an inner enum. */
  public enum InnerEnum {
    A,
    B,
    C
  }

  /** Example of an inner interface. */
  public interface Interface {

    /**
     * The field for testing, not used in code.
     *
     * @return the value as int.
     */
    int field();

    /** Example of a double nested interface. */
    interface NestedInterface {
      NestedEnum nestedEnum();

      /** Example of a triple nested enum. */
      enum NestedEnum {
        FIRST,
        SECOND,
        THIRD
      }
    }
  }

  /** Example of a nested record. */
  record InnerRecord(int field) implements Interface {}

  /** Example of a nested abstract static class. */
  private abstract static class AbstractClass implements Interface.NestedInterface {

    protected abstract NestedRecord getNestedRecord();

    @Override
    public Interface.NestedInterface.NestedEnum nestedEnum() {
      return getNestedRecord().nestedEnum();
    }

    /** Example of a nested record. */
    record NestedRecord(Interface.NestedInterface.NestedEnum nestedEnum) {}
  }

  /** Example of a nested final non-static class. */
  public final class FinalClass extends AbstractClass {
    @Override
    protected NestedRecord getNestedRecord() {
      return new NestedRecord(value);
    }
  }
}
