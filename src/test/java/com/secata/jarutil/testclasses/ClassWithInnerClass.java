package com.secata.jarutil.testclasses;

/** Test utility for {@link JarBuilderTest}. */
public class ClassWithInnerClass {

  /** Public field for testing. */
  public InnerClazz inner;

  /** Constructor for test class. */
  public ClassWithInnerClass() {
    this.inner = new InnerClazz();
  }

  /** Example of an inner class. */
  public static final class InnerClazz {

    InnerClazz() {}
  }
}
