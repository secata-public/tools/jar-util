package com.secata.jarutil;

import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A regular dependency defined in a pom file. */
public final class Dependency {

  private static final Logger logger = LoggerFactory.getLogger(Dependency.class);

  private final String groupId;
  private final String artifactId;
  private final String version;
  private final Type type;

  /**
   * Create a new dependency.
   *
   * @param groupId the group id.
   * @param artifactId the artifact id.
   * @param version the version.
   */
  public Dependency(String groupId, String artifactId, String version) {
    this(groupId, artifactId, version, Type.JAR);
  }

  /**
   * Create a new dependency.
   *
   * @param groupId the group id.
   * @param artifactId the artifact id.
   * @param version the version.
   * @param type the type of dependency (is it a test jar).
   */
  public Dependency(String groupId, String artifactId, String version, Type type) {
    this.groupId = groupId;
    this.artifactId = artifactId;
    this.version = version;
    this.type = type;
  }

  static Dependency parse(org.apache.maven.model.Dependency mavenDependency) {
    Type type = convertType(mavenDependency.getType());
    return new Dependency(
        mavenDependency.getGroupId(),
        mavenDependency.getArtifactId(),
        mavenDependency.getVersion(),
        type);
  }

  static Type convertType(String type) {
    if ("jar".equals(type)) {
      return Type.JAR;
    } else if ("test-jar".equals(type)) {
      return Type.TESTS;
    } else if ("pom".equals(type)) {
      return Type.POM;
    } else {
      throw new IllegalArgumentException("Cannot parse dependency type from: " + type);
    }
  }

  /**
   * Get the group id of the dependency.
   *
   * @return group id
   */
  public String getGroupId() {
    return groupId;
  }

  /**
   * The artifact id of the dependency.
   *
   * @return artifact id
   */
  public String getArtifactId() {
    return artifactId;
  }

  /**
   * Get the version of this dependency.
   *
   * @return the version string
   */
  public String getVersion() {
    return version;
  }

  /**
   * Get the type of this dependency.
   *
   * @return the {@link com.secata.jarutil.Dependency.Type} of this dependency
   */
  public Type getType() {
    return type;
  }

  /**
   * Get the name of the file for this artifact.
   *
   * @return the artifact file name
   */
  public String toJarFilename() {
    return String.format("%s-%s%s", getArtifactId(), getVersion(), getType().fileSuffix);
  }

  @Override
  public String toString() {
    String typeIndicator = type.equals(Type.TESTS) ? " (" + type + ")" : "";
    return "Dependency{'" + groupId + "." + artifactId + "." + version + "'" + typeIndicator + "}";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dependency that = (Dependency) o;
    return Objects.equals(groupId, that.groupId)
        && Objects.equals(artifactId, that.artifactId)
        && Objects.equals(version, that.version)
        && Objects.equals(type, that.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupId, artifactId, version, type);
  }

  /** Dependency type. */
  public enum Type {
    /** Type for a jar artifact. */
    JAR(".jar"),
    /** Type for a test-jar artifact. */
    TESTS("-tests.jar"),
    /** Type for a pom artifact. */
    POM(".pom"),
    /** Type for a sources artifact. */
    SOURCES("-sources.jar");

    final String fileSuffix;

    Type(String fileSuffix) {
      this.fileSuffix = fileSuffix;
    }
  }

  /**
   * Read this dependency from the local maven repository.
   *
   * <p>The location of the local repository is in order of priority one of:
   *
   * <ol>
   *   <li>The environment variable LOCAL_MAVEN_REPOSITORY
   *   <li>The environment variable MAVEN_OPTS using the value of the property -Dmaven.repo.local
   *   <li>Default local repository location ${user.home}/.m2/repository
   * </ol>
   *
   * @return the artifact bytes
   */
  public byte[] readFromRepository() {
    Path artifactPath = toRepositoryPath();
    return ExceptionConverter.call(() -> Files.readAllBytes(artifactPath), "IO error");
  }

  /**
   * Get the local path of the dependency.
   *
   * @return the path of the dependency.
   */
  public Path toRepositoryPath() {
    String groupPath = groupId.replaceAll("\\.", "/");
    return Path.of(
        MavenRepositoryHolder.LOCAL_REPOSITORY, groupPath, artifactId, version, toJarFilename());
  }

  static String resolveLocalRepository() {
    return resolveLocalRepository(System::getenv);
  }

  static String resolveLocalRepository(Function<String, String> environment) {
    String localMavenRepository = environment.apply("LOCAL_MAVEN_REPOSITORY");
    if (localMavenRepository != null) {
      return localMavenRepository;
    } else {
      String mavenOpts = environment.apply("MAVEN_OPTS");
      String repository = repositoryFromMavenOpts(mavenOpts);
      return Objects.requireNonNullElseGet(repository, Dependency::defaultLocalRepository);
    }
  }

  private static String defaultLocalRepository() {
    return System.getProperty("user.home") + "/.m2/repository";
  }

  private static String repositoryFromMavenOpts(String mavenOpts) {
    Pattern mavenRepoOption = Pattern.compile(Pattern.quote("-Dmaven.repo.local=") + "(\\S+)");
    if (mavenOpts != null) {
      Matcher matcher = mavenRepoOption.matcher(mavenOpts);
      if (matcher.find()) {
        return matcher.group(1);
      }
    }
    return null;
  }

  /**
   * Class to hold resolved local maven repository. In inner class to lazy initialize the value when
   * first needed.
   */
  private static class MavenRepositoryHolder {

    private static final String LOCAL_REPOSITORY;

    static {
      LOCAL_REPOSITORY = resolveLocalRepository();
      logger.info("Using maven repository {}.", LOCAL_REPOSITORY);
    }
  }
}
