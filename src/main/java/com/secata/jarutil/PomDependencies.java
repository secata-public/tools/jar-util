package com.secata.jarutil;

import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

/** Reader for Maven dependencies that are output by 'mvn dependency:tree'. */
public final class PomDependencies {

  private final String projectVersion;
  private final Model parentModel;
  private final Model model;

  private PomDependencies(Model parentModel, Model model) {
    this.parentModel = parentModel;
    this.model = model;

    String modelVersion = model.getVersion();
    if (modelVersion != null && parentModel != null) {
      this.projectVersion = parentModel.getVersion();
    } else {
      this.projectVersion = modelVersion;
    }

    resolvePropertyVersions();
  }

  private void resolvePropertyVersions() {
    Properties properties = new Properties();
    if (parentModel != null) {
      properties.putAll(parentModel.getProperties());
    }
    properties.putAll(model.getProperties());

    resolvePropertyVersions(properties, model);
    if (parentModel != null) {
      resolvePropertyVersions(properties, parentModel);
    }
  }

  private void resolvePropertyVersions(Properties properties, Model model) {
    for (var dependency : model.getDependencies()) {
      String version = dependency.getVersion();
      if (Objects.equals("${project.version}", version)) {
        dependency.setVersion(projectVersion);
      } else if (String.valueOf(version).startsWith("${")) {
        String resolvedVersion = resolvePropertyIfFound(properties, version);
        dependency.setVersion(resolvedVersion);
      }
    }
  }

  /**
   * Read the model from the given pom file.
   *
   * @param pom the pom file to read model from
   * @return the created instance
   */
  public static Model readModelFromPom(File pom) {
    MavenXpp3Reader reader = new MavenXpp3Reader();
    return WithResource.apply(
        () -> new FileReader(pom, StandardCharsets.UTF_8),
        reader::read,
        "IO Exception when reading model from pom file: " + pom.getPath());
  }

  /**
   * Read the dependencies from pom file in current working directory.
   *
   * @return the created instance
   */
  public static PomDependencies readFromPom() {
    return readFromPom(new File(".", "pom.xml"), false);
  }

  /**
   * Read the dependencies from pom file in current working directory.
   *
   * @param includeFromReactor whether to include dependencies from the reactor (in a multi-module
   *     setup)
   * @return the created instance
   */
  public static PomDependencies readFromPom(boolean includeFromReactor) {
    return readFromPom(new File(".", "pom.xml"), includeFromReactor);
  }

  /**
   * Read the dependencies from the given pom file.
   *
   * @param pom the pom file to read dependencies from
   * @return the created instance
   */
  public static PomDependencies readFromPom(File pom) {
    return readFromPom(pom, false);
  }

  /**
   * Read the dependencies from the given pom file.
   *
   * @param pom the pom file to read dependencies from
   * @param includeFromReactor whether to include dependencies from the reactor (in a multi-module
   *     setup)
   * @return the created instance
   */
  public static PomDependencies readFromPom(File pom, boolean includeFromReactor) {
    Model model = readModelFromPom(pom);

    if (includeFromReactor) {
      Model parentModel = readModuleParent(model, pom);
      return new PomDependencies(parentModel, model);
    } else {
      return new PomDependencies(null, model);
    }
  }

  /**
   * Attempt to resolve a property. If the property does not exist, return the property name.
   *
   * @param properties the properties object
   * @param propertyName the name of the property
   * @return the resolved property if found, the property name otherwise
   */
  private static String resolvePropertyIfFound(Properties properties, String propertyName) {
    String ref = propertyName.substring(2, propertyName.length() - 1);
    return Objects.requireNonNullElse(properties.getProperty(ref), propertyName);
  }

  private static Model readModuleParent(Model model, File modulePom) {
    Parent parent = model.getParent();
    if (parent != null) {
      File pomModuleFolder = modulePom.getParentFile();

      File file = new File(pomModuleFolder, parent.getRelativePath());
      if (file.isFile()) {
        return readModelFromPom(file);
      }
    }

    return null;
  }

  private static Dependency asDependency(Parent parent) {
    return new Dependency(
        parent.getGroupId(), parent.getArtifactId(), parent.getVersion(), Dependency.Type.POM);
  }

  /**
   * Gets the underlying model loaded from the pom file.
   *
   * @return the pom file loaded.
   */
  public Model model() {
    return model;
  }

  /**
   * Gets every dependency read from the file.
   *
   * @return the dependencies.
   */
  public List<Dependency> dependencies() {
    List<Dependency> dependencies =
        model.getDependencies().stream().map(Dependency::parse).collect(Collectors.toList());

    if (parentModel != null) {
      parentModel.getDependencies().stream().map(Dependency::parse).forEach(dependencies::add);

      Parent parent = parentModel.getParent();
      if (parent != null) {
        dependencies.add(asDependency(parent));
      }
    }

    Parent parent = model.getParent();
    if (parent != null) {
      dependencies.add(asDependency(parent));
    }

    return dependencies;
  }

  /**
   * Look up the matching dependency based on group id, artifact id.
   *
   * @param groupId the group id of the dependency
   * @param artifactId the artifact id of the dependency
   * @return the matching dependency
   */
  public Dependency lookup(String groupId, String artifactId) {
    return lookup(groupId, artifactId, Dependency.Type.JAR);
  }

  /**
   * Look up all the matching dependencies based on group id, artifact id.
   *
   * @param groupId the group id of the dependency
   * @param artifactId the artifact id of the dependency
   * @param type the type of jar to look for
   * @return the matching dependency
   */
  public Dependency lookup(String groupId, String artifactId, Dependency.Type type) {
    return this.dependencies().stream()
        .filter(dependency -> dependency.getGroupId().equals(groupId))
        .filter(dependency -> dependency.getArtifactId().equals(artifactId))
        .filter(dependency -> dependency.getType().equals(type))
        .findFirst()
        .orElse(null);
  }
}
