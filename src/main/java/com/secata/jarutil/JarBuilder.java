package com.secata.jarutil;

import com.google.errorprone.annotations.CheckReturnValue;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Utility for creating complete {@code .jar} file (as byte arrays) from some given input {@link
 * Class}es.
 *
 * <p>Primary use case is for creating jar files of contracts, from the contract repository, for
 * testing purposes.
 *
 * <p>Produced jar will contain:
 *
 * <ul>
 *   <li>Entry for supplied main class.
 *   <li>Entries for supplied additional classes.
 *   <li>Entries for nested classes of supplied classes, including recursively nested classes.
 *   <li>{@code main} entry indicating the supplied main class as entry point of contract.
 * </ul>
 */
public final class JarBuilder {

  private final Long zipEntryTimestamp;

  private JarBuilder(Long zipEntryTimestamp) {
    this.zipEntryTimestamp =
        Objects.requireNonNullElse(zipEntryTimestamp, System.currentTimeMillis() / 1000L);
  }

  /**
   * Builds a jar containing the supplied class as main class and any inner classes declared.
   *
   * @param mainClass the main contract class. Not nullable.
   * @param additionalClasses additional classes to write. Not nullable.
   * @return the jar. Not nullable.
   * @see JarBuilder for more documentation.
   */
  @CheckReturnValue
  public static byte[] buildJar(Class<?> mainClass, Class<?>... additionalClasses) {
    return buildJar(null, mainClass, additionalClasses);
  }

  /**
   * Builds a jar containing the supplied class as main class and any inner classes declared.
   *
   * @param zipEntryTimestamp the timestamp in seconds to set on the zip entries. Will default to
   *     current time if {@code null}.
   * @param mainClass the main contract class. Not nullable.
   * @param additionalClasses additional classes to write. Not nullable.
   * @return the jar. Not nullable.
   * @see JarBuilder for more documentation.
   */
  @CheckReturnValue
  public static byte[] buildJar(
      Long zipEntryTimestamp, Class<?> mainClass, Class<?>... additionalClasses) {
    return ExceptionConverter.call(
        () -> {
          ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
          JarBuilder builder = new JarBuilder(zipEntryTimestamp);
          builder.writeToStream(
              byteStream, mainClass, classesToWrite(mainClass, additionalClasses));

          return byteStream.toByteArray();
        },
        "Unable to create jar file");
  }

  /**
   * Determines the set of classes to be included in the jar file.
   *
   * @param mainClass Main class. Not nullable.
   * @param additionalClasses Additional classes. Not nullable.
   * @return Set of classes to include. Not nullable.
   */
  @CheckReturnValue
  private static Set<Class<?>> classesToWrite(
      final Class<?> mainClass, final Class<?>[] additionalClasses) {
    final Set<Class<?>> classAccumulator = new LinkedHashSet<Class<?>>();

    // Main class and nested
    classesToWriteInternal(classAccumulator, mainClass);
    for (final Class<?> clazz : additionalClasses) {
      classesToWriteInternal(classAccumulator, clazz);
    }
    return classAccumulator;
  }

  /**
   * Determines the set of classes to include in the class accumulator for the given {@link Class}.
   *
   * @param classAccumulator Output set of classes to include.
   * @param clazz Class to determine inclusion classes for.
   */
  private static void classesToWriteInternal(Set<Class<?>> classAccumulator, final Class<?> clazz) {
    classAccumulator.add(clazz);
    for (final Class<?> nestedClass : clazz.getDeclaredClasses()) {
      classesToWriteInternal(classAccumulator, nestedClass);
    }
  }

  private void writeToStream(OutputStream out, Class<?> mainClass, Iterable<Class<?>> allClasses)
      throws IOException {
    final ZipOutputStream jar = new ZipOutputStream(out);

    writeMainEntry(jar, mainClass);

    final Set<Path> emittedDirectories = new LinkedHashSet<>();

    for (final Class<?> clazz : allClasses) {
      final Path classPackageDirectory = Path.of(nameToPath(clazz.getName())).getParent();
      writeDirectoryAndParents(jar, emittedDirectories, classPackageDirectory);
      writeClass(jar, clazz.getName());
    }

    jar.close();
  }

  /**
   * Writes the directory structure, including parents if those have not already been emitted.
   *
   * @param jar Jar file to write to.
   * @param alreadyEmittedDirectories Set of directories that have already been written.
   * @param path Path to write.
   */
  private void writeDirectoryAndParents(
      ZipOutputStream jar, Set<Path> alreadyEmittedDirectories, Path path) throws IOException {
    if (!alreadyEmittedDirectories.contains(path)) {
      final Path parent = path.getParent();
      if (parent != null) {
        writeDirectoryAndParents(jar, alreadyEmittedDirectories, parent);
      }
      jar.putNextEntry(createEntry("%s/".formatted(path)));
      alreadyEmittedDirectories.add(path);
    }
  }

  private void writeMainEntry(final ZipOutputStream jar, final Class<?> mainClass)
      throws IOException {
    jar.putNextEntry(createEntry("main"));
    jar.write(mainClass.getName().getBytes(StandardCharsets.UTF_8));
  }

  private void writeClass(ZipOutputStream jar, String className) throws IOException {
    String classNameAsPath = nameToPath(className);
    String fileName = classNameAsPath + ".class";

    ZipEntry entry = createEntry(fileName);

    jar.putNextEntry(entry);
    WithResource.accept(
        () -> JarBuilder.class.getResourceAsStream("/" + fileName),
        resourceAsStream -> resourceAsStream.transferTo(jar),
        "Error");
  }

  private ZipEntry createEntry(String fileName) {
    ZipEntry entry = new ZipEntry(fileName);

    FileTime from = FileTime.from(zipEntryTimestamp, TimeUnit.SECONDS);
    entry.setLastModifiedTime(from);

    return entry;
  }

  @CheckReturnValue
  static String nameToPath(String className) {
    return className.replace('.', '/');
  }
}
